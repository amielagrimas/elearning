app.factory('ListFactory', function($http){
		var factory ={};

		factory.add_instructor = function(data){
			var promise = $http({
				url:'./FUNCTIONS/List/add_instructor.php',
				method: 'POST',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj){
					var str = [];
					for(var p in obj)
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
				},
				data : data
			})
			return promise;
		};

		factory.fetch_instructor = function(data){
			var promise = $http({
				url:'./FUNCTIONS/List/fetch_instructor.php',
				method: 'POST',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj){
					var str = [];
					for(var p in obj)
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
				},
				data : data
			})
			return promise;
		};

		factory.add_student = function(data){
			var promise = $http({
				url:'./FUNCTIONS/List/add_student.php',
				method: 'POST',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj){
					var str = [];
					for(var p in obj)
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
				},
				data : data
			})
			return promise;
		};

		factory.fetch_student = function(data){
			var promise = $http({
				url:'./FUNCTIONS/List/fetch_student.php',
				method: 'POST',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj){
					var str = [];
					for(var p in obj)
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
				},
				data : data
			})
			return promise;
		};
		return factory;
	});	