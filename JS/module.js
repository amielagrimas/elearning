var app = angular.module('onload', ['ngRoute']);

app.config(function($routeProvider){
        $routeProvider
        .when('/',
    {
        templateUrl: 'TEMPLATES/login.html'
    })
        .when('/Home',
    {
        controller: 'Home',
        templateUrl: 'TEMPLATES/home.html'
    })
       .when('/Chapter1',
    {
        controller: 'LessonSlide',
        templateUrl: 'TEMPLATES/Chapter1/chapter1.html'
    })
        .when('/Lesson1.1',
    {
        controller: 'LessonSlide',
        templateUrl: 'TEMPLATES/Chapter1/lesson1.1.html'
    })    
        .when('/Lesson1.2',
    {
        controller: 'LessonSlide',
        templateUrl: 'TEMPLATES/Chapter1/lesson1.2.html'
    })
        .when('/Lesson1.3',
    {
        controller: 'LessonSlide',
        templateUrl: 'TEMPLATES/Chapter1/lesson1.3.html'
    })
      .when('/Lesson1.4',
    {
        controller: 'LessonSlide',
        templateUrl: 'TEMPLATES/Chapter1/lesson1.4.html'
    })
        .when('/Lesson1.5',
    {
        controller: 'LessonSlide',
        templateUrl: 'TEMPLATES/Chapter1/lesson1.5.html'
    })
        .when('/Lesson1.6',
    {
        controller: 'LessonSlide',
        templateUrl: 'TEMPLATES/Chapter1/lesson1.6.html'
    })
        .when('/Lesson1.7',
    {
        controller: 'LessonSlide',
        templateUrl: 'TEMPLATES/Chapter1/lesson1.7.html'
    })
        .when('/Lesson2.1', 
    {
        controller: 'LessonSlide',
        templateUrl: 'TEMPLATES/Chapter2/lesson2.1.html'
    })
        .when('/About',
    {
        templateUrl: 'TEMPLATES/about.html'
    })
        .when('/Profile',
    {
        controller: 'Profile',
        templateUrl: 'TEMPLATES/profile.html'
    })
         .when('/InstructorList',
    {
        controller: 'Account',
        templateUrl: 'TEMPLATES/lists.html'
    })
    .when('/StudentList',
    {
        controller: 'List',
        templateUrl: 'TEMPLATES/studlists.html'
    })
        .when('/Reports',
    {
        controller: 'List',
        templateUrl: 'TEMPLATES/reports.html'
    })
    .otherwise({
            redirectTo: '/'
        })
});
