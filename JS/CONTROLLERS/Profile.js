app.controller('Profile', function ($scope,
                                        SessionFactory,
                                        UserFactory
 
)
{

	$scope.profile = {};

   init();
    function init(){
        var promise = SessionFactory.getsession();
        promise.then(function(data){
             var pk = ('pk');
            $scope.pk = data.data[pk]; 
      
        })
        .then(null, function(data){
            window.location = '#/Home';
        });
    }
    
    
    function get_user(){

        var promise = UserFactory.get_user();
        promise.then(function(data){
            $scope.profile = data.data.result[0];
            get_user();
        })
        .then(null, function(data){

        });
    }

    $scope.logout = function(){
        var promise = SessionFactory.logout();
        promise.then(function(data){
        window.location = '#/';
    })
}

 $scope.update_profile = function(){
 	var promise = UserFactory.update_profile();
        promise.then(function(data){
        	alert('You have succesfully changed your password');
            get_user();
        })
        .then(null, function(data){
           alert('Oops there is something wrong!');
        });
 	}

});