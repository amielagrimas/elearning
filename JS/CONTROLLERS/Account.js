app.controller('Account', function($scope,
                                    UserFactory,
                                    SessionFactory
                                    ){
                                 
    $scope.profile = {};   
    $scope.permissions = {};                                

     init();
    function init(){
        var promise = SessionFactory.getsession();
        promise.then(function(data){
            var pk = ('pk');
            $scope.pk = data.data[pk];
            get_user();
        })
        .then(null, function(data){
            window.location = '#/Home';
        });
    }

    $scope.logout = function(){
        var promise = SessionFactory.logout();
        promise.then(function(data){
            window.location = '#/';
        })
    } 

    function get_user(){
        var promise = UserFactory.get_user();
        promise.then(function(data){
            $scope.profile = data.data.result[0];
            get_instructor();

        })
        .then(null, function(data){

        });
    }
    
$scope.instructor = [];
instructor();
    function instructor(){
        var promise = UserFactory.get_instructor();
        promise.then(function(data){
            $scope.instructor = data.data.result;

        for (var x in $scope.instructor) {
            if ($scope.instructor[x].gender == 1) {
                $scope.instructor[x].gender = 'Female';
            };
        }; 
        for (var x in $scope.instructor) {
            if ($scope.instructor[x].gender == 2) {
                $scope.instructor[x].gender = 'Male';
            };
        }; 
        for (var x in $scope.instructor) {
            if ($scope.instructor[x].user_type == 3) {
                $scope.instructor[x].user_type = 'Instructor';
            };
        }; 
    })
}
$scope.add_instructor = function(){
        var promise = UserFactory.add_instructor($scope);
        promise.then(function(data){
            alert('You have succesfully added!');
            $scope = "";
            instructor();
        })
        .then(null, function(data){
            alert('Oops there is something wrong!');

        });
    };
  

$scope.edit_permission = function(value){
        $scope.permission = JSON.stringify($scope.permissions);
        var promise = UserFactory.edit_permission($scope);
        promise.then(function(data){
            alert('You have succesfully updated the user');
            get_user();
        })
        .then(null, function(data){
           alert('Oops there is something wrong!');
        });
}
  
        
});