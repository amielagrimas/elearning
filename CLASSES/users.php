<?php
require_once('../../CLASSES/ClassParent.php');
class Accounts extends ClassParent
{
 
public function get_account($post){
        $account_id = pg_escape_string(strip_tags(trim($post['account_id'])));
        $password = pg_escape_string(strip_tags(trim($post['password'])));
        $sql = <<<EOT
                select  
                    users.*
                from accounts   
                left join users on (accounts.acc_id = users.acc_id)
                where users.archived = 'f'
                and accounts.acc_id = '$account_id'
                and (accounts.password = '$password')
                ;
EOT;
        return ClassParent::get($sql);
    }  


    public function get_user($data){
        $pk = $data['pk'];
        $sql = <<<EOT
                select 
                    pk,
                    acc_id,
                    first_name,
                    user_type,
                    last_name,
                    permission,
                    gender,
                    (select password from accounts where acc_id = users.acc_id) as password,
                    date_created::timestamp(0),
                    archived
                from users
                where archived = 'f'
                ;
EOT;

        return ClassParent::get($sql);
    }


 public function get_instructor($data){
        foreach($data as $k=>$v){
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        } 

        $where = "";
        
        if($data['search'] != ''){
            $where .= " and first_name ILIKE '%".$data['search']."%' OR last_name ILIKE '%".$data['search']."%'";
        }else{
            $where = "";  
        }


        $sql = <<<EOT
            select *
                from instructor
                where archived = 'f'
                $where
                order by date_created asc
                ;
EOT;
        return ClassParent::get($sql);
    }
    public function get_student($data){
        foreach($data as $k=>$v){
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        } 

        $where = "";
        
        if($data['search'] != ''){
            $where .= " and first_name ILIKE '%".$data['search']."%' OR last_name ILIKE '%".$data['search']."%'";
        }else{
            $where = "";  
        }


        $sql = <<<EOT
           select * from student
                where archived = 'f'
                $where
                order by date_created asc
                ;
EOT;
        return ClassParent::get($sql);
    }

    public function add_instructor($data){
        $acc_id = $data['acc_id'];
        $first_name = $data['first_name'];
        $middle_name = $data['middle_name'];
        $last_name = $data['last_name'];
        $password = $data['password'];
        $course_section = $data['course_section'];
        $user_type = $data['user_type'];
        $gender = $data['gender'];

        $sql = "begin;";
        $sql .= <<<EOT
                insert into accounts
                (
                    acc_id,
                    password,
                    user_type
                )
                VALUES
                (
                    '$acc_id',
                    '$password',
                    '$user_type'

                )
                ;
EOT;
        $sql .= <<<EOT
                insert into users
                (
                    acc_id,
                    first_name,
                    middle_name,
                    last_name,
                    user_type,
                    gender
                )
                VALUES
                (
                    '$acc_id',
                    '$first_name',
                    '$middle_name',
                    '$last_name',
                    '$user_type',
                    '$gender'
                )
                ;
EOT;
        $sql .= <<<EOT
                insert into instructor
                (
                    acc_id,
                    first_name,
                    middle_name,
                    last_name,
                    course_section,
                    user_type,
                    gender
                )
                VALUES
                (
                    '$acc_id',
                    '$first_name',
                    '$middle_name',
                    '$last_name',
                    '$course_section',
                    '$user_type',
                    '$gender'
                )
                ; 
EOT;
        $sql .= "commit;";
        return ClassParent::insert($sql);
    }

    public function add_student($data){
        $acc_id = $data['acc_id'];
        $first_name = $data['first_name'];
        $middle_name = $data['middle_name'];
        $last_name = $data['last_name'];
        $password = $data['password'];
        $course_section = $data['course_section'];
        $user_type = $data['user_type'];
        $gender = $data['gender'];

        $sql = "begin;";
        $sql .= <<<EOT
                insert into accounts
                (
                    acc_id,
                    password,
                    user_type
                )
                VALUES
                (
                    '$acc_id',
                    '$password',
                    '$user_type'
                )
                ;
EOT;
        $sql .= <<<EOT
                 insert into users
                (
                    acc_id,
                    first_name,
                    middle_name,
                    last_name,
                    user_type,
                    gender
                )
                VALUES
                (
                    '$acc_id',
                    '$first_name',
                    '$middle_name',
                    '$last_name',
                    '$user_type',
                    '$gender'
                )
                ;
EOT;
        $sql .= <<<EOT
                insert into student
                (
                    acc_id,
                    first_name,
                    middle_name,
                    last_name,
                    course_section,
                    user_type,
                    gender
                )
                VALUES
                (
                    '$acc_id',
                    '$first_name',
                    '$middle_name',
                    '$last_name',
                    '$course_section',
                    '$user_type',
                    '$gender'
                )
                ; 
EOT;
        $sql .= "commit;";
        return ClassParent::insert($sql);
    }
     

    public function edit_permission($data){

        $acc_id = $data['acc_id'];
        $permission = $data['permission'];

        $tod = json_decode($permission,true);
        $toe = json_encode($tod);
        $sql = <<<EOT
                update users set permission = '$toe' where acc_id = '$acc_id'
                ; 
EOT;

        return ClassParent::update($sql);
    }

     public function update_password($data){
        foreach($data as $k=>$v){
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $acc_id = $data['acc_id'];
        $old_password = $data['old_password'];
        $password = $data['new_password'];

        $sql = <<<EOT
                update accounts set
                (password)
                =
                ('$password')
                where acc_id = '$acc_id'
                and password = '$old_password'
                ;
EOT;

        return ClassParent::update($sql);
    }

}
?>