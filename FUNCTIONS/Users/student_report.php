<?php
	require_once('../../PDF/fpdf.php');

	$data = json_decode($_GET['data'],true);
	class PDF extends FPDF
	{
		function construct ($margin = 20) 
		{ 
			$this->SetTopMargin($margin); 
			$this->SetLeftMargin($margin); 
			$this->SetRightMargin($margin); 
			$this->SetAutoPageBreak(true, $margin); 
		} 

		function Header()
		{
		    $this->SetFont('Arial', 'B', 16); 
			$this->SetFillColor(36, 96, 84); 
			$this->Cell(0, 1, "E-Payslip", 0, 0, 'L'); 
			$this->Image('../../ASSETS/img/aomoschrslogo.png',150,-2,50);
		    $this->Ln(20);
		}

		function Footer() { 
			$this->SetFont('Arial', '', 10); 
			$this->SetTextColor(0); 
			$this->SetXY(20,-20); 
			$this->Cell(0, 20, "Confidential", 'T', 0, 'L');
			$this->Cell(0, 20, "This is system auto-generated", 'T', 0, 'R');
			$this->SetXY(0,-10);
			$this->Cell(0, 10, "Powered By BSIT 4P-G1", '', 0, 'R'); 
		}

		function SetCellMargin($margin){
	        // Set cell margin
	        $this->cMargin = $margin;
	    }
	}
	$pdf = new PDF();
	$pdf->SetFont('Arial','',16);
	$pdf->setCellMargin(2);
	// $pdf-$column_widths = ['50','50','50','50'];
	$pdf->construct();
	$pdf->AddPage('P','A4');
	foreach ($data as $k => $v) {
		if ($v['finalpay'] > 0) {
			$_POST['currencypeso'] = 'PHP';
			// $pdf->Cell(85,6,'',1,0,'L',0);  
			$pdf->setFillColor(255,255,0);
			$pdf->Cell(170,12,'SPECIAL PAYROLL DETAILS','LTBR',0,'C',1);
			$pdf->Ln(17);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(170,6,'Final Pay for '.$v['fullsname'],'LTBR',0,'C',1);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(85,6,'Last Salary',0,0,'C',0);
			$pdf->Cell(85,6,round($v['last_pay'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,'13th Month Pay',0,0,'C',0);
			$pdf->Cell(42.5,6,'January',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['januarys'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'February',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['februarys'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'March',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['marchs'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'April',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['aprils'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'May',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['mays'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'June',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['junes'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'July',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['julys'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'August',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['augusts'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'September',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['septembers'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'October',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['octobers'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'November',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['novembers'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->Cell(42.5,6,'December','B',0,'C',0);
			$pdf->Cell(42.5,6,round($v['decembers'],2),'B',0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,' ',0,0,'C',0);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(42.5,6,'Total',0,0,'C',0);
			$pdf->Cell(42.5,6,round($v['thirteens'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(85,6,'Annualized Withholding Tax',0,0,'C',0);
			$pdf->Cell(85,6,round($v['tax'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,'Loan Deduction (SSS, PAG-IBIG)',0,0,'C',0);
			$pdf->Cell(85,6,'-'.round($v['loans'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,'Cash Advanced',0,0,'C',0);
			$pdf->Cell(85,6,'-'.round($v['cash'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,'Company Loan',0,0,'C',0);
			$pdf->Cell(85,6,'-'.round($v['com_loan'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,'Incentive Leave (turned to cash)',0,0,'C',0);
			$pdf->Cell(85,6,round($v['leaves'],2),0,0,'C',0);
			$pdf->Ln(10);
			$pdf->Cell(85,6,'Other Deductions','B',0,'C',0);
			$pdf->Cell(85,6,'-'.round($v['others'],2),'B',0,'C',0);
			$pdf->Ln(10);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(85,6,'Total Computation',0,0,'C',0);
			$pdf->Cell(85,6,round($v['finalpay'],2),0,0,'C',0);
			$pdf->Ln(10);
		}
	}
	$pdf->Output();
?>