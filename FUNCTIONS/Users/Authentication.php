<?php
require_once('../connect.php');
require_once('../../CLASSES/users.php');
$class = new Accounts(
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,
						NULL
					);

$data = $class->get_account($_POST);

header("HTTP/1.0 404 User Not Found");
if($data['status']){ 
	//print_r($data);
	$pk = ('pk'); 
	setcookie($pk, ($data['result'][0]['pk']), time()+7200000, '/');
	header("HTTP/1.0 200 OK");
}

header('Content-Type: application/json');
print(json_encode($data));
?>