/*USERNAME AND PASSWORD OF POSTGRES*/
create user team with password 'capstone';
	
/*CREATE DATABASE*/
create database learn;

CREATE TABLE accounts (
    pk serial primary key,
    acc_id text,
    password text,
    user_type text
);
alter table accounts owner to team;

CREATE TABLE users (
    pk serial primary key,
    acc_id text NOT NULL,
    first_name text NOT NULL,
    middle_name text NOT NULL,
    last_name text NOT NULL,
    user_type text NOT NULL,
    permission jsonb,
    date_created timestamp with time zone DEFAULT now(),
    archived boolean DEFAULT false
);
ALTER TABLE users OWNER TO team;

CREATE TABLE instructor (
    pk serial primary key,
    acc_id text NOT NULL,
    first_name text NOT NULL,
	middle_name text NOT NULL,
    last_name text NOT NULL,
    course_section text NOT NULL,
    user_type text NOT NULL,
    date_created timestamp with time zone DEFAULT now(),
    archived boolean DEFAULT false
);
ALTER TABLE instructor OWNER TO team;

CREATE TABLE student (
    pk serial primary key,
    acc_id text NOT NULL,
    first_name text NOT NULL,
	middle_name text NOT NULL,
    last_name text NOT NULL,
    course_section text NOT NULL,
    user_type text NOT NULL,
    date_created timestamp with time zone DEFAULT now(),
    archived boolean DEFAULT false
);
ALTER TABLE student OWNER TO team;


/*jsonb*/
{"Admin":{"Modules":{"One":true,"Two":true,"Three":true}}}
